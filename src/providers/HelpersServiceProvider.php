<?php

namespace Helpers\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Scheduling\Schedule;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        Validator::extend('idCard', function ($attribute, $value, $parameters, $validator) {
            if(!ctype_digit($value)){
                return false;
            }
            if (strlen($value) < 9) {
                $value = str_pad($value, 9, "0", STR_PAD_LEFT);
            }

            if ($value == '000000000')
                return false;

            for($i = 0; $i < 9; $i++){
                if($i % 2 != 0){
                    $c = $value[$i] * 2;

                    if($c > 9){
                        $value[$i] = ($c % 10) + 1;
                    } else {
                        $value[$i] = $c;
                    }
                }
            }
            if( (array_sum( str_split($value) )) % 10 > 0){
                return false;
            }

            return true;
        });

        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/(0)[0-9]{1,2}(-)[0-9]{7}/', $value);
        });
    }


}
